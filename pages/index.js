import NavBar from "../components/NavBar";
import Presentation from "../components/Presentation";
import About from "../components/About";
import Services from "../components/services/Services";
import Stats from "../components/Stats";
import Portfolio from "../components/Portfolio";
import Testimonials from "../components/Testimonials";
import Available from "../components/Available";
import Contact from "../components/contact/Contact";
import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <div className="font-regular min-h-screen min-w-screen bg-site-background text-primary overflow-y-hidden">
      <Head>
        <title>Dylan Belouis Portfolio</title>
        <meta
          name="description"
          content="Découvrez le portfolio de Dylan Belouis, un développeur Fullstack passionné depuis des années, spécialisé dans les technologies React, Next.js, TypeScript, Django et Node.js. Parcourez ses réalisations remarquables, où il allie son expertise en développement front-end et back-end pour créer des applications Web innovantes et performantes. Explorez ses projets soigneusement conçus, démontrant sa maîtrise des meilleures pratiques de développement, son sens de l'esthétique et son souci du détail. Plongez dans l'univers de Dylan, où la créativité rencontre l'efficacité, pour découvrir comment il transforme les idées en produits numériques de qualité supérieure. Laissez-vous inspirer par son engagement inébranlable envers l'excellence technique et sa passion inépuisable pour le développement."
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <NavBar />
      <div className="mb-20">
        <Presentation />
      </div>
      <div className="mt-56">
        <About />
      </div>
      <Services />
      <div className="relative w-full flex justify-center ">
        <div className={styles.pngLights}></div>
        <Stats />
      </div>
      <Portfolio />
      <Testimonials />
      <Available />
      <Contact />
    </div>
  );
}
