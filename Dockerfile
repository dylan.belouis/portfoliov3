# Étape 1 : Installer les dépendances de développement
FROM node:14-alpine AS development
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
COPY . .

# Étape 2 : Exécuter l'application en mode de développement
FROM development AS dev
ENV NODE_ENV development
CMD ["npm", "run", "dev"]

# Étape 3 : Exécuter l'application en mode de production
FROM development AS production
ENV NODE_ENV production
RUN npm run build
CMD ["npm", "run", "start"]