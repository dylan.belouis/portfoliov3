import React from "react";
import Image from "next/image";
import Link from "next/link";
const About = () => {
  return (
    <section
      id="about"
      className="mx-40 my-5 mt-40 md:mt-40 flex flex-row justify-around"
      style={{
        paddingTop: "140px !important",
      }}
    >
      <div className="md:hidden flex min-w-1/2">
        <div className="relative h-1/2 w-full opacity-60">
          <Image
            layout="fill"
            src="/assets/dylan_about.webp"
            alt="dylan belouis"
          />
        </div>
        {/* <></> */}
      </div>
      <div className="lg:pl-10 w-screen lg:min-w-1/2">
        <div className="flex flex-col justify-center relative w-full text-center md:text-left">
          {/* <div className="absolute md:relative w-full min-h-full"> */}
          <h2
            className="relative min-h-full w-full text-4xl md:text-6xl font-bold md:mb-20"
            style={{
              marginBottom: "40px !important",
            }}
          >
            Qui est ce ?
          </h2>
          {/* </div> */}
          <div className="relative md:mt-40 mt-12 md:flex hidden h-1/2 w-full">
            <Image
              layout="fill"
              src="/assets/dylan_about.webp"
              alt="dylan belouis web developper"
              priority={1}
            />
          </div>
        </div>
        <p className="text-l lg:max-w-lg lg:leading-8 lg:text-xl mt-6 lg:mt-0 mb-4 mx-10 lg:mx-0">
          Dylan, 25 ans, passionné par les technologies informatiques. Je suis
          un développeur Fullstack passionné par la création d&apos;applications
          web performantes et évolutives. J&apos;ai une expertise dans le
          développement côté front-end et back-end, et je m&apos;intéresse
          constamment aux dernières tendances technologiques grâce à une veille
          régulière.{" "}
        </p>
        <br />
        <p className="text-l lg:max-w-lg lg:leading-8 lg:text-xl mt-6 lg:mt-0 mb-16 mx-10 lg:mx-0">
          En 3 mots, je suis passionné, curieux et investi. J&apos;aime
          par-dessus tout le partage de connaissances et travailler en équipe
          pour livrer des produits de qualité dans les meilleurs délais. Je mets
          toujours tout en œuvre pour que mes projets soient réussis et mes
          clients satisfaits. 🚀
        </p>
        <p className="text-l lg:text-xl mt-2 lg:mt-0 mb-6 mx-10 lg:mx-0">
          Soft-Skills :
        </p>
        <p className="text-l lg:text-xl mb-16 mx-10 lg:mx-0">
          #Professionel #Passionnée #Autonome #Heureux #CodeAddict #Sociable
        </p>
        <p className="text-l lg:text-xl mt-6 lg:mt-0 mb-6 mx-4 lg:mx-0">
          Orientation de mon profil :
        </p>
        <p className="mb-6 ml-4 lg:ml-0">FrontEnd 65%</p>
        <div className="relative pt-1 mb-16 mx-4 lg:mx-0">
          <div className="overflow-hidden h-1 mb-4 text-xs flex rounded bg-gray-700">
            <div className="shadow-none w-full mr-thirty_five flex flex-col text-center whitespace-nowrap text-white justify-center bg-site-violet"></div>
          </div>
        </div>
        <h6 className="mb-6 ml-4 lg:ml-0">BackEnd et Bases de données 35%</h6>
        <div className="relative pt-1 mb-16 mx-4 lg:mx-0">
          <div className="overflow-hidden h-1 mb-4 text-xs flex rounded bg-gray-700">
            <div className="shadow-none w-full mr-sixty_five flex flex-col text-center whitespace-nowrap text-white justify-center bg-site-violet"></div>
          </div>
        </div>
        <div className="mx-4 flex justify-row lg:mx-0">
          <Link href="#services">
            <div
              // onClick={() => scrollTo("#services")}
              className="rounded-md mr-6 bg-site-violet py-6 w-60 item-center text-center cursor-pointer"
            >
              <p>En savoir plus</p>
            </div>
          </Link>
          <Link href="#form">
            <div
              // onClick={() => scrollTo("#form")}
              className="rounded-md border-white border-2 py-6 w-60 item-center text-center cursor-pointer"
            >
              <p>Embauchez moi</p>
            </div>
          </Link>
        </div>
      </div>
    </section>
  );
};

export default About;
