import React from "react";
import Image from "next/image";

const Stats = ({ yfdhc }) => {
  return (
    // <div className="flex flex-rows justify-around mt-40 mx-20">
    <section
      className="md:relative absolute pb-60 grid grid-cols-2 text-center mt-10 lg:mx-56 lg:grid-cols-4"
      style={{
        // paddingTop: "100px",
        position: "absolute",
        zIndex: "8",
        top: 400,
      }}
    >
      <div className="py-28 border-2 border-dashed border-white rounded-md flex flex-col items-center bg-site-secondary m-4">
        <p className="font-bold text-lg lg:text-6xl">05+</p>
        <p className="text-xl lg:text-4xl">Clients</p>
      </div>
      <div className=" py-28 rounded-md flex flex-col items-center bg-site-violet m-4">
        <p className="font-bold lg:text-6xl">20</p>
        <p className="lg:text-4xl">Projets</p>
      </div>
      <div className=" py-28 border-2 border-dashed border-gray-50 rounded-md flex flex-col items-center bg-site-secondary m-4">
        <div className="w-20 h-auto">
          <Image
            width={"100%"}
            height={"100%"}
            layout="responsive"
            src="/assets/infiniteIcon.webp"
            alt="passionate icon on dylan belouis développeur website"
          />
        </div>
        <p className="lg:text-4xl">passionnée</p>
      </div>
      <div className=" py-28 border-2 border-dashed border-white rounded-md flex flex-col items-center bg-site-secondary m-4">
        <p className="font-bold text-lg lg:text-6xl">03</p>
        <p className="text-xl lg:text-4xl">ans d&apos;Exp</p>
      </div>
    </section>
  );
};

export default Stats;
