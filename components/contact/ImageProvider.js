import React from "react";
import Image from "next/image";

const ImageProvider = ({ stack }) => {
  const Provider = (props) => {
    switch (props.value) {
      case "HTML":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              width={"80%"}
              height={"80%"}
              layout="responsive"
              src="/images/stack/html-5.png"
              alt="dejhfe"
            />
          </div>
        );
      case "React":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              width={"80%"}
              height={"80%"}
              layout="responsive"
              src="/images/stack/React.webp"
              alt="dejhfe"
            />
          </div>
        );
      case "Gatsby":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              width={"80%"}
              height={"80%"}
              layout="responsive"
              src="/images/stack/Gatsby.webp"
              alt="dejhfe"
            />
          </div>
        );
      case "Next":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              width={"80%"}
              height={"80%"}
              layout="responsive"
              src="/images/stack/nextjs.jpg"
              alt="dejhfe"
            />
          </div>
        );
      case "CSS3":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              width={"80%"}
              height={"80%"}
              layout="responsive"
              src="/images/stack/css-3.png"
              alt="dejhfe"
            />
          </div>
        );
      case "TailwindCSS":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              width={"80%"}
              height={"80%"}
              layout="responsive"
              src="/images/stack/tailwindcss.png"
              alt="dejhfe"
            />
          </div>
        );
      case "StyledComponent":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              width={"80%"}
              height={"80%"}
              layout="responsive"
              src="/images/stack/styledComponents.jpeg"
              alt="dejhfe"
            />
          </div>
        );
      case "MaterialUI":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              width={"80%"}
              height={"80%"}
              layout="responsive"
              src="/images/stack/materialui.png"
              alt="dejhfe"
            />
          </div>
        );
      case "Firebase":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              width={"80%"}
              height={"80%"}
              layout="responsive"
              src="/images/stack/firebase.png"
              alt="dejhfe"
            />
          </div>
        );
      case "PostgreSQL":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              width={"80%"}
              height={"80%"}
              layout="responsive"
              src="/images/stack/postgresql.jpg"
              alt="dejhfe"
            />
          </div>
        );
      case "MySQL":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              width={"80%"}
              height={"80%"}
              layout="responsive"
              src="/images/stack/mysql.png"
              alt="dejhfe"
            />
          </div>
        );
      case "Node":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              width={"80%"}
              height={"80%"}
              layout="responsive"
              src="/images/stack/Node.webp"
              alt="dejhfe"
            />
          </div>
        );
      case "Express":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              width={"80%"}
              height={"80%"}
              layout="responsive"
              src="/images/stack/Express.webp"
              alt="dejhfe"
            />
          </div>
        );
      case "Django":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              width={"80%"}
              height={"80%"}
              layout="responsive"
              src={"/images/stack/django.png"}
              alt="dejhfe"
            />
          </div>
        );
      case "Docker":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              width={"80%"}
              height={"80%"}
              layout="responsive"
              src="/images/stack/docker.png"
              alt="dejhfe"
            />
          </div>
        );
      case "Eslint":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              width={"80%"}
              height={"80%"}
              layout="responsive"
              src="/images/stack/Express.webp"
              alt="dejhfe"
            />
          </div>
        );
      case "Prettier":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              width={"80%"}
              height={"80%"}
              layout="responsive"
              src="/images/stack/React.webp"
              alt="dejhfe"
            />
          </div>
        );
      case "Git":
        return (
          <div className="w-20 h-20 max-w-20 max-h-20">
            <Image
              src="/images/stack/git.png"
              alt="dejhfe"
              width={"80%"}
              height={"80%"}
            />
          </div>
        );
      default:
        return console.log("probleme");
    }
  };

  return (
    <>
      <Provider value={stack} />
    </>
  );
};

export default ImageProvider;
