import React, { useState, useEffect } from "react";
// import scrollTo from "gatsby-plugin-smoothscroll";
import Image from "next/image";
import Link from "next/link";

const Navbar = () => {
  const [isOpen, setIsOpen] = useState(false);

  const mobileScroll = (path) => {
    setIsOpen(!isOpen);
    // scrollTo(path);
  };

  useEffect(() => {}, [isOpen]);
  return (
    <div>
      <div className="bg-site-background z-40 w-full maw-w-screen fixed h-20 lg:h-36">
        <nav className="flex lg:h-full items-center flex-rows justify-between">
          <div className="flex items-center">
            <div className="w-16 h-auto mt-2 lg:mt-0 lg:w-32 lg:h-auto">
              <Image
                width={"80%"}
                height={"80%"}
                layout="responsive"
                src="/assets/logo.webp"
                alt="logo dylan belouis website"
              />
            </div>
          </div>
          <div className="h-full justify-end text-white lg:flex hidden">
            <Link href="#about">
              <div
                //   onClick={() => scrollTo("#about")}
                className="h-full cursor-pointer bg-site-violet flex justify-center items-end p-10"
              >
                <p>A propos</p>
              </div>
            </Link>
            <Link href="#services">
              <div
                //   onClick={() => scrollTo("#services")}
                className="transition duration-500 h-full cursor-pointer hover:bg-site-violet flex justify-center items-end lg:p-10"
              >
                <p>Services</p>
              </div>
            </Link>
            <Link href="#portfolio">
              <div
                //   onClick={() => scrollTo("#portfolio")}
                className="transition duration-500 h-full cursor-pointer hover:bg-site-violet flex justify-center items-end lg:p-10"
              >
                <p>Mes projets</p>
              </div>
            </Link>
            <Link href="#testimonials">
              <div
                //   onClick={() => scrollTo("#testimonials")}
                className="transition duration-500 ease-in-out h-full cursor-pointer hover:bg-site-violet flex justify-center items-end lg:p-10"
              >
                <p>Les avis</p>
              </div>
            </Link>
            <Link href="#contact">
              <div
                //   onClick={() => scrollTo("#contact")}
                className="transition duration-500 cursor-pointer hover:bg-site-violet flex justify-center items-end lg:p-10"
              >
                <p>Contact</p>
              </div>
            </Link>
          </div>
          <div
            onClick={() => setIsOpen(!isOpen)}
            className="w-10 h-full lg:hidden mr-10 items-center"
          >
            <Image
              width={"10%"}
              height={"10%"}
              layout="responsive"
              src="/assets/menu.webp"
              alt="navbar of dylan belouis developpeur website"
            />
          </div>
        </nav>
      </div>
      <div
        className={
          isOpen
            ? `flex flex-col text-white w-screen h-screen z-30 pt-24 text-center bg-site-background fixed`
            : `hidden`
        }
      >
        <div onClick={() => mobileScroll("#about")} className="">
          <p className="my-10">A propos</p>
        </div>
        <div onClick={() => mobileScroll("#services")} className="">
          <p className="my-10">Services</p>
        </div>
        <div onClick={() => mobileScroll("#portfolio")} className="">
          <p className="my-10">Mes projets</p>
        </div>
        <div onClick={() => mobileScroll("#testimonials")} className="">
          <p className="my-10">Les avis</p>
        </div>
        <div onClick={() => mobileScroll("#contact")} className="">
          <p className="my-10">Contact</p>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
