import React from "react";
import Image from "next/image";

const Presentation = () => {
  return (
    <section
      id="presentation"
      className="relative lg:mt-20 lg:pt-20 mb-56 bg-opacity-25 h-full section"
    >
      <div className="flex lg:hidden opacity-50 z-0 h-full w-full presentation_image"></div>
      <div className="top-0 h-full absolute flex flex-col justify-center z-10 opacity-100 mx-10 lg:mx-0 lg:ml-40 mb-32 lg:mb-0 mt-10 bg-transparent">
        <div className="relative my-8 w-36 h-12 bg-site-violet flex justify-center items-center mt-auto">
          <div
            style={{
              bottom: "-15px",
            }}
            className="absolute right-1/2 bottom-0 w-0 h-0 border-8 border-transparent border-t-site-violet"
          ></div>
          <p className="text-xl">Hello, je suis</p>
        </div>

        <h2 className="lg:my-8 text-xl lg:text-7xl font-bold text-white text-start">
          Dylan Belouis
        </h2>
        <p className="my-8 text-xl max-w-3/4">
          Un développeur fullstack passionnée
        </p>
        <div className="flex justify-row">
          <div className="opacity-100 rounded-md mr-6 bg-site-violet py-6 px-6 lg:w-60 item-center text-center cursor-pointer">
            <a href="/assets/cvDylanBelouis.pdf" download>
              Mon CV
            </a>
          </div>
          <div className="rounded-md border-white border-2 py-6 px-6 lg:w-60 item-center text-center cursor-pointer">
            <p>Qui suis-je</p>
          </div>
        </div>
      </div>
      <div className="relative z-0 w-full h-presentation_mobile  lg:h-full lg:w-full flex justify-end ml-auto opacity-40">
        <div
          className="relative lg:w-1/2  w-presentation_mobile lg:h-full"
          style={{
            height: "100vh",
          }}
        >
          <Image
            width={"100%"}
            height={"100%"}
            layout="responsive"
            src="/assets/dylan.webp"
            alt="dylan belouis developper"
          />
        </div>
      </div>
    </section>
  );
};

export default Presentation;
